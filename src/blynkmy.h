#ifndef BLYNKMY_H
#define BLYNKMY_H

#include <Arduino.h>
#include <BlynkSimpleEsp8266.h>
#include "logger.h"
#include "config.h"
#include "sysinfo.h"
#include "credentials.h"
#include "s7.h"

class BlynkMy
{
public:
  static long buttonOnOfMillis;

  static void setup()
  {

    Blynk.config(BLYNK_TOKEN, BLYNK_HOST, BLYNK_PORT);
  }

  static void handle()
  {

    Blynk.run();
  }

  static void log(String msg, uint level = 6)
  {
    LoggerUdp::log(msg);
  }
};

long BlynkMy::buttonOnOfMillis = 0;

BLYNK_CONNECTED()
{
  BlynkMy::log("Blynk connected to host: " + String(BLYNK_HOST) + ", port: " + String(BLYNK_HOST) + ", token: " + String(BLYNK_TOKEN), 6);
  S7::announce();
}

BLYNK_WRITE(InternalPinOTA)
{
  LoggerUdp::log("InternalOta update requested");
  auto overTheAirURL = param.asString();
  LoggerUdp::log(overTheAirURL);
};

#endif
