#ifndef LOGGER_H
#define LOGGER_H


#include <Arduino.h>
#include <FS.h>
#include "timetools.h"
#include "config.h"
//#include "gpioswitch.h"


//You need SPIFFS to be ready - SPIFFS.begin();
class Logger {
    public:
        static bool syslog(String msg,String fileName);
};

bool Logger::syslog(String msg, String fileName = LOGGER_FILE_NAME) {

    if (msg.length() > LOGGER_LINE_MAX_LENGTH)
  {
    msg = "Log Message over limit: " + String(LOGGER_LINE_MAX_LENGTH) + ". Will be cutted. \n" + msg.substring(0, LOGGER_LINE_MAX_LENGTH);
  }

  msg = Timetools::formattedDateTime() + " " + msg;
    
  File f = SPIFFS.open(fileName, "a+");
  if (!f)
  {
    if (LOGGER_USE_SERIAL) Serial.println(fileName + " open failed");
    return false;
  }
  f.println(msg);

  //rotation thing
  if (f.size() > LOGGER_FILE_SIZE)
  {
    String prevFileName = fileName.substring(0,fileName.lastIndexOf('.')) + "." +LOGGER_PREV_EXT;
    f.println("File over LOGGER_FILE_SIZE:" + String(LOGGER_FILE_SIZE) + ", will be renamed to " + prevFileName + ", old one removed if exists");
    if (SPIFFS.exists(prevFileName)) SPIFFS.remove(prevFileName);
    f.close();
    SPIFFS.rename(fileName, prevFileName);
  }
  if (f)
    f.close();
    if (LOGGER_USE_SERIAL) Serial.println(msg);

    return true;
}

#endif