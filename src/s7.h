#ifndef S7_H
#define S7_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESPAsyncUDP.h>
#include <map>
#include <vector>
#include "logger.h"

#define S7_1_WIRE_PIN 2 //D4
#define S7_BROADCAST_INTERVAL 500
#define S7_LISTEN_PORT 3333
#define S7_TARGET "s7"
#define S7_FROM "from"
#define S7_ZONE_KEY "zone"
#define S7_TO_KEY "to"
#define S7_RFID_KEY "rfid"
#define S7_CHANGE_STATE_DOWN_DURATION 300
#define S7_ALARM_STATE_KEY "alarmState"
#define S7_SERIAL_MAX_LENGTH 500
#define S7_SERIAL_DATA_CHUNKS_COUNT 20
#define S7_BLYNK_ZONE_LED_START_VPIN 1
#define S7_BLYNK_ARM_ZONE_START_VPIN 31
#define S7_BLYNK_DISARM_ZONE_START_VPIN 41
#define S7_BLYNK_ARM_ALL_VPIN 10
#define S7_BLYNK_DISARM_ALL_VPIN 11
#define S7_BLYNK_ZONE_UPDATE_TIME_VPIN 13
#define S7_BLYNK_INPUT_START_VPIN 21
#define S7_ZONE_COUNT 8
#define S7_INPUT_COUNT 8
#define S7_NO_VALID_CMD_TIMEOUT 60*3*1000

class S7
{
public:
    static ulong lastStatusTime;
    static AsyncUDP udp;
    static bool initCompleted;
    static ulong changeStateRequestTime;
    static bool zones[S7_ZONE_COUNT];
    static bool zonesBlynk[S7_ZONE_COUNT];
    static bool inputs[S7_INPUT_COUNT];
    static bool inputsBlynk[S7_ZONE_COUNT];
    static std::map<int, String> serialDataRxLast;
    static WiFiClient tcpClient;
    static String packetReceivedLast;
    static String packetSentLast;
    static long lastValidSerialCommandReceived;

    static void setup()
    {
        if (udp.listenMulticast(IPAddress(239, 1, 1, 1), S7_LISTEN_PORT))
        {
            log("Listening on port:" + String(S7_LISTEN_PORT));
            udp.onPacket([](AsyncUDPPacket packet) {
                String packetData = "";
                for (size_t i = 0; i < packet.length(); i++)
                {
                    packetData += (char)*(packet.data() + i);
                }
                String m = "UDP Packet Type: ";
                m += packet.isBroadcast() ? "Broadcast" : packet.isMulticast() ? "Multicast" : "Unicast";
                m += ", From: " + packet.remoteIP().toString() + ":" + String(packet.remotePort()) + ", To: " + packet.localIP().toString() + ":";
                m += String(packet.localPort()) + ", Length: " + String(packet.length()) + ", Data: " + packetData;
                log(m, LOG_INFO);
                packetReceivedLast = packetData;

                StaticJsonDocument<512> doc;
                DeserializationError error = deserializeJson(doc, packetData);
                if (error)
                {
                    log("Failed to parse JSON.", LOG_ERR);
                    return;
                }
                if (doc[S7_TO_KEY].isNull() || doc[S7_RFID_KEY].isNull() || doc[S7_ZONE_KEY].isNull())
                {
                    log("Keys S7_TO_KEY:" + String(S7_TO_KEY) + " or S7_RFID_KEY: " + String(S7_RFID_KEY) + " or S7_ZONE_KEY: " + String(S7_ZONE_KEY) + " missing", LOG_ERR);
                    return;
                }

                if (doc[S7_TO_KEY].as<String>() != S7_TARGET)
                {
                    log("I'm not the target, dropping packet.", LOG_INFO);
                    return;
                }
                bool userFound = false;
                for (auto &rfid : RFID_ALLOWED)
                {
                    if (rfid.key == doc[S7_RFID_KEY].as<String>())
                    {
                        if (doc[S7_ZONE_KEY].as<int>() <1 || doc[S7_ZONE_KEY].as<int>()> S7_ZONE_COUNT) {
                            log("zone number: " + doc[S7_ZONE_KEY].as<String>() + " does not exists", LOG_ERR);
                        }
                        log("Requested to change state by: " + rfid.name + ", key: " + rfid.key + ", zone: " + String(doc[S7_ZONE_KEY].as<int>()) + ", zoneArray: " +  String(doc[S7_ZONE_KEY].as<int>()-1), LOG_INFO);
                        zoneChangeState(doc[S7_ZONE_KEY].as<int>()-1);
                        announce();
                        userFound = true;
                    }
                }

                if (!userFound) log("I'm unable to match received key: " + doc[S7_RFID_KEY].as<String>() + " to known users", LOG_ERR);
            });
        }

        log("AT: Sending ready", LOG_INFO);
        Serial.println("ready");

        //tcpClient.keepAlive(10);
        if (!tcpClient.connect(S7_BACKEND_HOSTNAME, S7_BACKEND_PORT)) {
            log("TCP connection failed",LOG_ERR);
        } else {
            log("TCP connection succeded",LOG_ERR);
        }
    }

    static void tcpSendPersistent(String msg) {
         if (tcpClient.connected()) {
            size_t s = tcpClient.println(msg);
            log("TCP connected, message sent, size: " + String(s) + ", content: " + msg, LOG_DEBUG);
         } else {
                 if (tcpClient.connect(S7_BACKEND_HOSTNAME, S7_BACKEND_PORT)) {
                 size_t s = tcpClient.println(msg);
                 log("TCP reconnected, message sent, size: " + String(s) + ", content: " + msg, LOG_DEBUG);
             } else {
                 log("TCP reconnect FAILED", LOG_DEBUG);
             }
         } 
    }

    static void processIncomingByte(const byte inByte)
    {
        static char input_line[S7_SERIAL_MAX_LENGTH];
        static unsigned int input_pos = 0;
        switch (inByte)
        {
        case '\n':                     // end of text
            input_line[input_pos] = 0; // terminating null byte
            // terminator reached! process input_line here ...
            processATCommand(input_line);
            // reset buffer for next time
            input_pos = 0;
            break;
        case '\r': // discard carriage return
            break;
        default:
            // keep adding if not full ... allow for terminating null byte
            if (input_pos < (S7_SERIAL_MAX_LENGTH - 1))
                input_line[input_pos++] = inByte;
            break;
        } // end of switch
    }

    static void processATCommand(const char *cmd)
    {
        String cmdStr = String(cmd);

        if (cmdStr == "AT" || cmdStr == "\\0AT")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            Serial.println("OK");
            lastValidSerialCommandReceived = millis();
            return;
        }
        if (cmdStr == "ATE0")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            Serial.println("OK");
            lastValidSerialCommandReceived = millis();
            return;
        }
        if (cmdStr == "AT+GMR")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            Serial.println("OK");
            lastValidSerialCommandReceived = millis();
            return;
        }
        if (cmdStr == "AT+CWMODE=1")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("OK");
            return;
        }
        if (cmdStr.indexOf("AT+CWJAP") != -1)
        {
            log("S7 wants to use following wifi credentials: " + cmdStr, LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("WIFI CONNECTED\nWIFI GOT IP\nOK");
            return;
        }

        if (cmdStr == "AT+CIPMODE=0")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("OK");
            return;
        }

        if (cmdStr == "AT+CIPMUX=0")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("OK");
            return;
        }
        if (cmdStr.indexOf("AT+CIPSTART=") != -1)
        {
            log("S7 makes TCP connection", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("CONNECT\nOK");
            return;
        }

        if (cmdStr == "AT+CIPSENDEX=2048" || cmdStr == "\\0AT+CIPSENDEX=2048")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("OK");
            return;
        }

        if (cmdStr == "AT+CIPCLOSE" || cmdStr == "\\0AT+CIPCLOSE")
        {
            log("AT: " + cmdStr + ", sending OK", LOG_DEBUG);
            lastValidSerialCommandReceived = millis();
            Serial.println("OK");
            return;
        }

        if (cmdStr.indexOf("{") != -1 && cmdStr.indexOf("}") != -1)
        {
            lastValidSerialCommandReceived = millis();
            log("Got actual S7 data: " + cmdStr, LOG_INFO);
            parseS7Data(cmdStr);
            Serial.println("Recv " + String(cmdStr.length()) + " bytes\nSEND OK");
            if (timeStatus() == timeSet)
                Serial.println("+IPD,43:COMMAND:;\nSERVER CLOCK: " + Timetools::s7formattedDateTime());
            return;
        }

        log("Unkown AT cmd: " + cmdStr + ", length: " + String(cmdStr.length()), LOG_ERR);
    }

    static void parseS7Data(String d)
    {
        tcpSendPersistent(d);

        d = d.substring(d.indexOf("{") + 1, d.indexOf("}"));
        int posLast = 0;
        for (int i = 1; i < S7_SERIAL_DATA_CHUNKS_COUNT; i++)
        {
            int pos = d.indexOf(",", posLast);
            String chunk = d.substring(posLast, pos);
            serialDataRxLast[i] = chunk;
            posLast = pos + 1;
        }

        char *endptr;
        long n;
        
        //parse zone state
        n = strtol(serialDataRxLast[3].substring(2, 4).c_str(), &endptr, 16);
        for (int i = S7_ZONE_COUNT - 1; i >= 0; --i)
        {
            ((n >> i) & 1) == 0 ? zones[i] = false : zones[i] = true;
        }
        
        //parse inputs
        n = strtol(serialDataRxLast[4].substring(2, 4).c_str(), &endptr, 16);
        String m = "";
        for (int i = S7_ZONE_COUNT - 1; i >= 0; --i)
        {
            ((n >> i) & 1) == 0 ? inputs[i] = false : inputs[i] = true;
        }
        announce();
    }

    static void handle()
    {
        if (millis() - lastStatusTime > S7_BROADCAST_INTERVAL && WiFi.isConnected())
        {
            StaticJsonDocument<512> doc;
            for (int i = 0; i < S7_ZONE_COUNT; i++)
            {
                doc["zone" + String(i+1)] = zones[i];
            }
            doc[S7_FROM] = S7_TARGET;
            String p;
            serializeJson(doc, p);
            udp.print(p.c_str());
            lastStatusTime = millis();
            packetSentLast = p;
        }

        while (Serial.available() > 0)
        {
            processIncomingByte(Serial.read());
        }

        if (tcpClient.available()) {
            String tcpData = "";
            while (tcpClient.available()) {
                char ch = static_cast<char>(tcpClient.read());
                tcpData += String(ch);
            }
            log("Got TCP data: " + tcpData, LOG_DEBUG);
            Serial.println(tcpData);
        }

        if (millis()-lastValidSerialCommandReceived > S7_NO_VALID_CMD_TIMEOUT) {
            // log("millis() - lastValidSerialCommandReceived:  " + String((millis()-lastValidSerialCommandReceived)/1000) + " over the allowed limit : " + String(S7_NO_VALID_CMD_TIMEOUT/1000) + " secods, will try to Serial.Flush()!" , LOG_ERR);
            log("millis() - lastValidSerialCommandReceived:  " + String((millis()-lastValidSerialCommandReceived)/1000) + " over the allowed limit : " + String(S7_NO_VALID_CMD_TIMEOUT/1000) + " secods, will restart!" , LOG_ERR);
            lastValidSerialCommandReceived = millis();
            espRestartRequest = true;
            
            // Serial.end();
            // Serial.begin(SERIAL_BAUD_RATE);
            // Serial.flush();
        }
    }

    static void announce()
    {
        for (int i = 0; i < S7_ZONE_COUNT; i++)
        {
            if (zonesBlynk[i] != zones[i]) {
                zones[i] ? Blynk.virtualWrite(S7_BLYNK_ZONE_LED_START_VPIN + i, 255) : Blynk.virtualWrite(S7_BLYNK_ZONE_LED_START_VPIN + i, 0);
                zonesBlynk[i] = zones[i];
            }
        }

        for (int i = 0; i < S7_INPUT_COUNT; i++)
        {
            if (inputsBlynk[i] != inputs[i]) {   
                inputs[i] ? Blynk.virtualWrite(S7_BLYNK_INPUT_START_VPIN + i, 255) : Blynk.virtualWrite(S7_BLYNK_INPUT_START_VPIN + i, 0);
                inputsBlynk[i] = inputs[i];
            }
        }

        Blynk.virtualWrite(S7_BLYNK_ZONE_UPDATE_TIME_VPIN , Timetools::formattedDateTime());
    }
    static void zoneChangeState(int id) {
        if (zones[id]) {
            String cmd = "COMMAND:" + String(S7_PIN) + "00" + String(id+1) + ";";
            Serial.println(cmd);
        } else {
            String cmd = "COMMAND:" + String(S7_PIN) + "01" + String(id+1) + ";";
            Serial.println(cmd);
        }
    }

    static void log(String msg, uint level = 6)
    {
        LoggerUdp::logToQueue(msg, level);
    }

    static String getWebInfo() {
        String t = "";
        t += "<strong>--- S7: </strong>";
        t += "\n";
        for (int i = 0; i < S7_ZONE_COUNT; i++) {
            t+= "zone #" + String(i+1) + ": " + String(zones[i]) + "\n";
        }
        t += "lastSentPacket: " + packetSentLast + "; ";
        t += "lastStatusTime: " + String(Timetools::formattedDateTimeFromMillis(lastStatusTime)) + "; ";
        t += "\n";
        t += "lastReceivedPacket: " + packetReceivedLast + ";";
        t += "\n";
        t += " millis() - lastValidSerialCommandReceived:  " + String((millis()-lastValidSerialCommandReceived)/1000) + ";";
        return t;
    }
};

ulong S7::lastStatusTime = 0;
bool S7::initCompleted = false;
AsyncUDP S7::udp;
ulong S7::changeStateRequestTime = 0;
bool S7::zones[8];
bool S7::inputs[8];
bool S7::zonesBlynk[8];
bool S7::inputsBlynk[8];
std::map<int, String> S7::serialDataRxLast;
WiFiClient S7::tcpClient;
String S7::packetReceivedLast = "";
String S7::packetSentLast = "";
long S7::lastValidSerialCommandReceived = 0;

BLYNK_WRITE(S7_BLYNK_ARM_ALL_VPIN)
{
    int temp = param.asInt();
    if (temp == 1)
    {
        String cmd = "COMMAND:" + String(S7_PIN) + "01;";
        S7::log("BLYNK requested ARM ALL, cmd:" + cmd, LOG_INFO);
        Serial.println(cmd);
    }
}

BLYNK_WRITE(S7_BLYNK_DISARM_ALL_VPIN)
{
    int temp = param.asInt();
    if (temp == 1)
    {
        String cmd = "COMMAND:" + String(S7_PIN) + "00;";
        S7::log("BLYNK requested DISARM ALL, cmd:" + cmd, LOG_INFO);
        Serial.println(cmd);
    }
}

BLYNK_WRITE_DEFAULT()
{
    int pin = request.pin;     // Which exactly pin is handled?
    int value = param.asInt(); // Use param as usual.

    //arm specific zone
    if (pin >= S7_BLYNK_ARM_ZONE_START_VPIN && pin < S7_BLYNK_ARM_ZONE_START_VPIN + S7_ZONE_COUNT && value == 1) {
        String cmd = "COMMAND:" + String(S7_PIN) + "01" + String(pin - S7_BLYNK_ARM_ZONE_START_VPIN + 1) + ";";
        S7::log("BLYNK requested to ARM zone: " + String(pin - S7_BLYNK_ARM_ZONE_START_VPIN +1 )  + ", cmd:" + cmd, LOG_INFO);
        Serial.println(cmd);
        return;
    }

    //disarm specific zone
    if (pin >= S7_BLYNK_DISARM_ZONE_START_VPIN && pin < S7_BLYNK_DISARM_ZONE_START_VPIN + S7_ZONE_COUNT && value == 1) {
        String cmd = "COMMAND:" + String(S7_PIN) + "00" + String(pin - S7_BLYNK_DISARM_ZONE_START_VPIN + 1) + ";";
        S7::log("BLYNK requested to DISARM zone: " + String(pin - S7_BLYNK_DISARM_ZONE_START_VPIN + 1)  + ", cmd:" + cmd, LOG_INFO);
        Serial.println(cmd);
        return;
    }
}

#endif