#ifndef CREDENTIALS_H
#define CREDENTIALS_H

#include <Arduino.h>

#define WIFI_DEFAULT_SSID "WIFI"
#define WIFI_DEFAULT_PASS "PASS"

struct RfidStruct {
    String name;
    String key;
    
};

#define BLYNK_HOST "blynk.hostname"
#define BLYNK_PORT 8090
#define BLYNK_TOKEN "token" //dev



const RfidStruct RFID_ALLOWED[] = {
    {"user","rfidkey"},
    };

#define GELF_UDP_LOGGER_HOSTNAME "graylog.hostname"
#define GELF_UDP_LOGGER_PORT 12201
#define GELF_UDP_LOGGER_MAX_LENGTH 1024
#define GELF_QUEUE_MAX_SIZE 10


#define S7_PIN 1234
#define S7_BACKEND_HOSTNAME "s7.hostname"
#define S7_BACKEND_PORT 31200


#endif